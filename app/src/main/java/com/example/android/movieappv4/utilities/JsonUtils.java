package com.example.android.movieappv4.utilities;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Deals with JSON.
 */

public final class JsonUtils {
    private final static String TAG = JsonUtils.class.getSimpleName();

    //The general token needed to split a given list
    private final static String TK_SPLIT = "results";

    //Check if Server loads
    private final static String CHECK_PAGE = "page";
    private final static String CHECK_ID = "id";

    //Videos
    private final static String TRAILER_TYPE = "type";//check if == "Trailer"
    public final static String TRAILER_KEY = "key";//append to YouTube url
    private final static String TRAILER_SITE = "site";//check if == "YouTube"


    /**
     * Converts a JSONObject into a String[], where each element is split by a token,
     * and given a specific order based off of the chosen token
     * @param json  json-String
     * @return      a String[] of the items which were split by the token
     * @throws JSONException
     */
    public static String[] getStringsFromJson(String json) throws JSONException{
        if(json == null || json.isEmpty()) return null;

        String[] parsedData;

        JSONObject jsonObject = new JSONObject(json);

        //If there exists no page, then quit; By default there is at least one
        //Or if there is no id, which is needed for trailers
        if (!(jsonObject.has(CHECK_PAGE) || jsonObject.has(CHECK_ID))){
            Log.w(TAG,"Server is probably down!");
            return null;
        }

        //an array that holds all the information of each movie
        JSONArray jsonArray = jsonObject.getJSONArray(TK_SPLIT);

        parsedData = new String[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++){
            //parsing movie(i) to a StringArray
            parsedData[i] = jsonArray.getJSONObject(i).toString();
        }
        return parsedData;
    }

    /**
     * Goes through all videos in the json and checks if they are a trailer and YouTube compatible.
     * @param json  json of videos
     * @return      the smallest array containing only YouTube compatible trailers
     * @throws JSONException
     */
    public static String[] getTrailersFromVideos(String[] json) throws JSONException{
        if (json == null){return null;}
        String[] videoPlaceHolder = new String[json.length];
        int minLength = 0;
        String[] videosWithYoutubeTrailer;

        //checks if the json-element has a trailer and can be watched on YouTube
        for (int i = 0,j=0; i < json.length; i++){
            JSONObject jsonObject = new JSONObject(json[i]);
            if (jsonObject.getString(TRAILER_TYPE).equals("Trailer") && jsonObject.getString(TRAILER_SITE).equals("YouTube")){
                videoPlaceHolder[j] = json[i];
                j++;
                minLength = j;
            }
        }

        //creates the smallest array
        videosWithYoutubeTrailer = new String[minLength];
        for (int i = 0; i<minLength;i++){
            videosWithYoutubeTrailer[i] = videoPlaceHolder[i];
        }

        return videosWithYoutubeTrailer;
    }
}
