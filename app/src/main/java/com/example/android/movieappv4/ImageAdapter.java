package com.example.android.movieappv4;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.movieappv4.database.DatabaseHandler;
import com.example.android.movieappv4.utilities.LogUtils;
import com.example.android.movieappv4.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.Arrays;

/**
 * ImageAdapter for the GridView.
 */

public class ImageAdapter extends BaseAdapter {
    private final static String TAG = ImageAdapter.class.getName();
    private String tempStr;
    private String[] searchResults;
    private Context mContext;
    private MovieSimple[] movieSimples;
    private DatabaseHandler mDb;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public ImageAdapter(Context c, String[] searchResults) throws JSONException {
        this.mContext = c;
        this.searchResults = searchResults;

        tempStr = Arrays.toString(searchResults);
        Log.v(TAG, "data_received = " + tempStr);

        this.movieSimples = new MovieSimple[searchResults.length];

        for (int i = 0; i < movieSimples.length; i++) {
            movieSimples[i] = new MovieSimple(searchResults[i]);
            tempStr = Arrays.toString(movieSimples[i].getMovieSimpleInfo());
            Log.v(TAG, "movie_simples(" + i + ") = " + tempStr);
        }
    }

    public ImageAdapter(Context context, DatabaseHandler db){
        this.mContext = context;
        this.mDb = db;
        this.movieSimples = db.getAllFavorites();
        if (movieSimples == null){
            Toast.makeText(mContext, "There are no Favorites to display.", Toast.LENGTH_SHORT).show();
        }
    }

    public MovieSimple getItem(int position) {
        if (position > movieSimples.length - 1) {
            return null;
        }
        return movieSimples[position];
    }

    public long getItemId(int position) {
        return 0;
    }

    //create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            //if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
//            imageView.setLayoutParams(new GridView.LayoutParams(85,85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        Uri movieImageUri = null;
        if (searchResults != null)
            movieImageUri = NetworkUtils.buildImageUri(movieSimples[position].getMovie_poster());
        Picasso.with(mContext).load(movieImageUri)
                .placeholder(R.drawable.broken_image)
                .error(android.R.drawable.stat_notify_error)
                .into(imageView);

        setOnClickListener(imageView, position);

        return imageView;
    }

    private void setOnClickListener(ImageView imageView, final int position) {

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MovieSimple movieData = null;
                try {
                    if (searchResults != null){
                        movieData = new MovieSimple(searchResults[position]);
                        LogUtils.stringLog("movie_id",movieData.getId(),TAG);
                    }else if (movieSimples != null){
                        movieData = movieSimples[position];
                    }else {
                        Toast.makeText(mContext,"Please try again with internet connection",Toast.LENGTH_SHORT).show();
                        return;
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "An error occurred in JSON", e);
                }

                Intent intent = new Intent(mContext, ChildActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, movieData);
                mContext.startActivity(intent);
            }
        });
    }

    public int getCount() {
        //first refer to movieSimples
        if (movieSimples != null && movieSimples.length > 0) {
            return movieSimples.length;
        }
        //and only if movieSimples is null or empty should we refer to mThumbIds
        return mThumbIds.length;
    }

    //reference to our images
    private Integer[] mThumbIds = {
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7
    };

}
