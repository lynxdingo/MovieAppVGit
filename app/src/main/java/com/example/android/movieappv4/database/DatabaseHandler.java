package com.example.android.movieappv4.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.android.movieappv4.MovieSimple;
import com.example.android.movieappv4.utilities.LogUtils;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private final static String TAG = DatabaseHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "favoritesMovieManager.db";

    // Contacts table name
    private static final String TABLE_FAVORITES = DatabaseContract.DatabaseEntry.TABLE_NAME;

    // Contacts Table Columns names
    private static final String COLUMN_ID = DatabaseContract.DatabaseEntry.COLUMN_ID;
    private static final String COLUMN_TITLE = DatabaseContract.DatabaseEntry.COLUMN_TITLE;
    private static final String COLUMN_RELEASE_DATE = DatabaseContract.DatabaseEntry.COLUMN_RELEASE_DATE;
    private static final String COLUMN_MOVIE_POSTER = DatabaseContract.DatabaseEntry.COLUMN_MOVIE_POSTER;
    private static final String COLUMN_VOTE_AVERAGE = DatabaseContract.DatabaseEntry.COLUMN_VOTE_AVERAGE;
    private static final String COLUMN_PLOT_SYNOPSIS = DatabaseContract.DatabaseEntry.COLUMN_PLOT_SYNOPSIS;
    private static final String COLUMN_FAVORITE = DatabaseContract.DatabaseEntry.COLUMN_FAVORITE;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE "
                + TABLE_FAVORITES + " ("
                + DatabaseContract.DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_ID + " INTEGER,"
                + COLUMN_TITLE + " TEXT,"
                + COLUMN_RELEASE_DATE + " TEXT,"
                + COLUMN_MOVIE_POSTER + " TEXT,"
                + COLUMN_VOTE_AVERAGE + " TEXT,"
                + COLUMN_PLOT_SYNOPSIS + " TEXT,"
                + COLUMN_FAVORITE + " TEXT"
                + " )";
        db.execSQL(CREATE_CONTACTS_TABLE);

        LogUtils.completedLog("onCreate",TAG);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITES);

        // Create tables again
        onCreate(db);
        db.close();
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new movieSimple
    public void addMovieSimple(MovieSimple movieSimple) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, Integer.valueOf(movieSimple.getId())); // MovieSimple Id
        values.put(COLUMN_TITLE, movieSimple.getTitle()); // MovieSimple Title
        values.put(COLUMN_RELEASE_DATE, movieSimple.getRelease_date()); // MovieSimple Release Date
        values.put(COLUMN_MOVIE_POSTER, movieSimple.getMovie_poster()); // MovieSimple Movie Poster
        values.put(COLUMN_VOTE_AVERAGE, movieSimple.getVote_average()); // MovieSimple Vote Average
        values.put(COLUMN_PLOT_SYNOPSIS, movieSimple.getPlot_synopsis()); // MovieSimple Plot Synopsis
        values.put(COLUMN_FAVORITE, "false"); // MovieSimple Favorite

        // Inserting Row
        db.insert(TABLE_FAVORITES, null, values);
        db.close(); // Closing database connection
    }

    public boolean inFavorite(String title){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_FAVORITES, new String[] {COLUMN_TITLE,COLUMN_FAVORITE},COLUMN_TITLE + " = ?",
                new String[] {title},null,null,null);

        try {
            if (cursor.moveToFirst()) {
                return cursor.getString(cursor.getColumnIndex(COLUMN_FAVORITE)).equals("true");
            }
        }catch (CursorIndexOutOfBoundsException e){
            Log.e(TAG,"Cursor error",e);
        }
        cursor.close();
        return false;
    }


    // Getting All MovieSimples
    public List<MovieSimple> getAllMovieSimples() {
        List<MovieSimple> movieList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FAVORITES;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MovieSimple movieSimple = new MovieSimple();
                movieSimple.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                movieSimple.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                movieSimple.setRelease_date(cursor.getString(cursor.getColumnIndex(COLUMN_RELEASE_DATE)));
                movieSimple.setMovie_poster(cursor.getString(cursor.getColumnIndex(COLUMN_MOVIE_POSTER)));
                movieSimple.setVote_average(cursor.getString(cursor.getColumnIndex(COLUMN_VOTE_AVERAGE)));
                movieSimple.setPlot_synopsis(cursor.getString(cursor.getColumnIndex(COLUMN_PLOT_SYNOPSIS)));
                movieSimple.setFavorite(cursor.getString(cursor.getColumnIndex(COLUMN_FAVORITE)));//all true
                // Adding movieSimple to list
                movieList.add(movieSimple);
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return movieSimple list
        return movieList;
    }

    public MovieSimple[] getAllFavorites(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_FAVORITES, new String[] { COLUMN_ID,
                        COLUMN_TITLE, COLUMN_RELEASE_DATE, COLUMN_MOVIE_POSTER, COLUMN_VOTE_AVERAGE,
                        COLUMN_PLOT_SYNOPSIS, COLUMN_FAVORITE }, COLUMN_FAVORITE + "=?",
                new String[] { "true" }, null, null, null, null);
        List<MovieSimple> movieList = new ArrayList<>();

        if (cursor.moveToFirst()){
            do {
                MovieSimple movieSimple = new MovieSimple();
                movieSimple.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                movieSimple.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                movieSimple.setRelease_date(cursor.getString(cursor.getColumnIndex(COLUMN_RELEASE_DATE)));
                movieSimple.setMovie_poster(cursor.getString(cursor.getColumnIndex(COLUMN_MOVIE_POSTER)));
                movieSimple.setVote_average(cursor.getString(cursor.getColumnIndex(COLUMN_VOTE_AVERAGE)));
                movieSimple.setPlot_synopsis(cursor.getString(cursor.getColumnIndex(COLUMN_PLOT_SYNOPSIS)));
                movieSimple.setFavorite(cursor.getString(cursor.getColumnIndex(COLUMN_FAVORITE)));//all true
                // Adding movieSimple to list
                movieList.add(movieSimple);
            }while (cursor.moveToNext());
        }
        cursor.close();

        if (movieList.size() > 0){//if there exists favorites
            MovieSimple[] movieSimples = new MovieSimple[movieList.size()];
            for (int i = 0; i<movieList.size();i++) {
                movieSimples[i]=movieList.get(i);
            }

            return movieSimples;
        }else{
            return null;
        }

    }

    // Updating single movieSimple
    public void updateMovieSimple(MovieSimple movieSimple) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_FAVORITE, movieSimple.getFavorite());

        // updating row
        db.update(TABLE_FAVORITES,values,COLUMN_TITLE + " = ?",
                new String[] {movieSimple.getTitle() });
        db.close();
    }

    //inserting a MovieSimple
    public void insertMovieSimple(MovieSimple movieSimple){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID,movieSimple.getId());
        values.put(COLUMN_TITLE, movieSimple.getTitle());
        values.put(COLUMN_RELEASE_DATE, movieSimple.getRelease_date());
        values.put(COLUMN_MOVIE_POSTER, movieSimple.getMovie_poster());
        values.put(COLUMN_VOTE_AVERAGE, movieSimple.getVote_average());
        values.put(COLUMN_PLOT_SYNOPSIS, movieSimple.getPlot_synopsis());
        LogUtils.stringLog("getFavorite",movieSimple.getFavorite(),TAG);
        values.put(COLUMN_FAVORITE, movieSimple.getFavorite());

        db.insert(TABLE_FAVORITES,null,values);
        db.close();
    }

    /**
     * Checks if the movie already exists within the database
     * @param title movie title
     * @return existence = true
     */
    public boolean existMovieSimple(String title){
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_FAVORITE,"false");

        int checkExistence = db.update(TABLE_FAVORITES,values,COLUMN_TITLE + " = ?",
                new String[] {title });

        return checkExistence >= 1;

    }



    // Deleting single movieSimple
    public void deleteMovieSimple(MovieSimple movieSimple) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FAVORITES, COLUMN_TITLE + " = ?",
                new String[] { String.valueOf(movieSimple.getTitle()) });
        db.close();
    }


    // Getting contacts Count
    public int getMovieSimpleCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FAVORITES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
}