package com.example.android.movieappv4.database;

import android.provider.BaseColumns;
/**
 * Created by DHPW on 30.05.2017.
 */

public class DatabaseContract {

    public static final class DatabaseEntry implements BaseColumns {
        public static final String TABLE_NAME = "FAVORITE_DB";
        public static final String COLUMN_ID = "MOVIE_ID";
        public static final String COLUMN_TITLE = "TITLE";
        public static final String COLUMN_RELEASE_DATE = "RELEASE_DATE";
        public static final String COLUMN_MOVIE_POSTER = "MOVIE_POSTER";
        public static final String COLUMN_VOTE_AVERAGE = "VOTE_AVERAGE";
        public static final String COLUMN_PLOT_SYNOPSIS = "PLOT_SYNOPSIS";
        public static final String COLUMN_FAVORITE = "FAVORITE";
    }
}
