package com.example.android.movieappv4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import com.example.android.movieappv4.database.DatabaseHandler;
import com.example.android.movieappv4.utilities.JsonUtils;
import com.example.android.movieappv4.utilities.NetworkUtils;

import org.json.JSONException;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.gridview) GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        new DatabaseHandler(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        String[] movieInfo = chooseSortingOption(String.valueOf(R.id.action_popular));

        if (movieInfo == null){
            gridView.setAdapter(new ImageAdapter(this));
        }else{
            try {
                gridView.setAdapter(new ImageAdapter(this, movieInfo));
            }catch (JSONException e){
                Log.e(TAG, "A Json error has occurred!",e);
            }
        }
    }


    //Adds the items to the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //Runs an action when item is selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MovieQueryTask onOptionTask = new MovieQueryTask(MainActivity.this,gridView);
        String menuItemThatWasSelected = String.valueOf(item.getItemId());
        onOptionTask.execute(menuItemThatWasSelected, NetworkUtils.BUILD_MOVIE_URL);

        return super.onOptionsItemSelected(item);
    }

    /**
     * Gets the json-data of the specific menu item, only if it doesn't time out after 300s
     * @param menuOptionId id value of the specific menu item
     * @return the movie data of the specific menu item
     */
    private String[] chooseSortingOption(String menuOptionId){
        String asyncTaskResult;
        try {
            asyncTaskResult = new MovieQueryTask()
                    .execute(menuOptionId,NetworkUtils.BUILD_MOVIE_URL)
                    .get(300, TimeUnit.SECONDS);
            Log.v(TAG,"async_task_results = "+asyncTaskResult);
            return JsonUtils.getStringsFromJson(asyncTaskResult);
        }catch(Exception e){
            Log.e(TAG, "Process timed out!",e);
            return null;
        }
    }
}
