package com.example.android.movieappv4.utilities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.example.android.movieappv4.BuildConfig;
import com.example.android.movieappv4.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Deals with all network related functions, especially Uri and Url builders..
 */

public class NetworkUtils {
    private final static String TAG = NetworkUtils.class.getSimpleName();//Tag reference

    private final static String MOVIE_BASE_URL = "http://api.themoviedb.org/3/";

    private final static String MOVIE_BASE_IMAGE = "http://image.tmdb.org/t/p/";

    private final static String MOVIE_POPULAR = "popular";
    private final static String MOVIE_RATED = "top_rated";

    private final static String API_KEY = BuildConfig.API_KEY;

    private final static String IMAGE_SIZE = "w185";//Other possibilities: w92, w154, w185, w342, w500, w780

    //YouTube
    private final static String YOUTUBE_URL = "https://www.youtube.com/watch";
    private final static String YOUTUBE_QUERY = "v";

    //Params
    private final static String PARAM_KEY = "api_key";
    private final static String PARAM_MOVIE = "movie";
    private final static String PARAM_REVIEWS = "reviews";
    private final static String PARAM_VIDEOS = "videos";

    //Options
    public final static String BUILD_MOVIE_URL = "100";
    public final static String BUILD_REVIEWS_URL = "200";
    public final static String BUILD_VIDEOS_URL = "300";
    public final static String BUILD_TRAILER_URL = "301";
    /**
     * Builds an Url based on the option selected.
     * Options:
     *          100: buildMovieUrl
     *          101: buildMovieUrl sorted by popular
     *          102: buildMovieUrl sorted by rated
     *          103: buildMovieUrl sorted by favorite
     *          200: buildReviewsUrl
     *          300: buildVideosUrl
     *          301: buildTrailerUrl
     * @param option build mode
     * @return generated url
     */
    public static URL buildGeneralUrl(int option, String info){
        URL builtUrl = null;
        switch (option){
            case 100://buildMovieUrl
                builtUrl = buildMovieUrl(info);
                break;
            /*case 101://buildMovieUrl by popular
                builtUrl = buildMovieUrl(info);
                break;
            case 102://buildMovieUrl by rated
                builtUrl = buildMovieUrl(info);
                break;
            case 103://buildMovieUrl by favorite
                builtUrl = buildMovieUrl(info);
                break;*/
            case 200://buildReviewsUrl
                builtUrl = buildReviewsUrl(info);
                break;
            case 300://buildVideosUrl
                builtUrl = buildVideosUrl(info);
                break;
            case 301://buildTrailerUrl
                builtUrl = buildTrailerUrl(info);
                break;
            default:
                Log.e(TAG,"Error wrong option selected!");
                break;
        }
        return builtUrl;
    }

    /**
     * Builds the URL used to query The Movie DataBase.
     *
     * Remark:
     *   query:=MOVIE_BASE_URL+PARAM_MOVIE+sort+PARAM_KEY+API_KEY
     *   image:=MOVIE_BASE_IMAGE+IMAGE_SIZE+poster_path
     *   where poster_path is retrieved from the query
     *
     * @param theMovieSettingOption The ID of the setting of in which one should sort.
     * @return The URL to use to query the weather server.
     */
    public static URL buildMovieUrl(String theMovieSettingOption) {
        Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                .appendPath(PARAM_MOVIE).appendPath(chooseSortingOption(theMovieSettingOption))
                .appendQueryParameter(PARAM_KEY,API_KEY)
                .build();
        return convertUriToUrl(builtUri);
    }

    /**
     * Builds the Uri for images.
     * @param imageStr the image_path that needs to be built.
     * @return the Uri of the image_path
     */
    public static Uri buildImageUri(String imageStr){
        Uri builtUri = Uri.parse(MOVIE_BASE_IMAGE).buildUpon()
                .appendPath(IMAGE_SIZE)
                .appendEncodedPath(imageStr)
                .build();
        return builtUri;
    }

    /**
     * Builds the Url, where the reviews for a specific movie are held.
     * @param movieId the movie's id
     * @return url of reviews location
     */
    public static URL buildReviewsUrl(String movieId){
        Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                .appendPath(PARAM_MOVIE)
                .appendPath(movieId)
                .appendPath(PARAM_REVIEWS)
                .appendQueryParameter(PARAM_KEY,API_KEY)
                .build();
        return convertUriToUrl(builtUri);
    }

    /**
     * Builds the Url, where the videos for a specific movie are held.
     * @param movieId the movie's id
     * @return url of videos location
     */
    public static URL buildVideosUrl(String movieId){
        Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                .appendPath(PARAM_MOVIE)
                .appendPath(movieId)
                .appendPath(PARAM_VIDEOS)
                .appendQueryParameter(PARAM_KEY,API_KEY)
                .build();
        return convertUriToUrl(builtUri);
    }

    /**
     * Builds the URL for a YouTube trailer.
     * @param trailerKey
     * @return the Url for the trailer
     */
    public static URL buildTrailerUrl(String trailerKey){
        Uri builtUri = Uri.parse(YOUTUBE_URL).buildUpon()
                .appendQueryParameter(YOUTUBE_QUERY,trailerKey)
                .build();
        return convertUriToUrl(builtUri);
    }

    /**
     * Converts an Uri to an Url.
     * @param builtUri an Uri to be converted
     * @return the converted Uri
     */
    private static URL convertUriToUrl(Uri builtUri){
        URL url = null;
        try{
            url = new URL(builtUri.toString());
//            Log.i(TAG,"url = "+url.toString());
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return url;
    }

    /**
     * This method returns the entire result from the HTTP response.
     *
     * @param url The URL to fetch the HTTP response from.
     * @return The contents of the HTTP response.
     * @throws IOException Related to network and stream reading
     */
    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }

    /**
     * Returns the given setting option.
     *
     * @param idStr chosen setting option
     * @return sorting option
     */
    private static String chooseSortingOption(String idStr){
        String sortBy;
        //technically redundant, but the switch would need to be fixed
        int id = Integer.parseInt(idStr);
        switch(id){
            case R.id.action_popular:
                sortBy = NetworkUtils.MOVIE_POPULAR;
                break;
            case R.id.action_rated:
                sortBy = NetworkUtils.MOVIE_RATED;
                break;
            default:
                sortBy = "ERROR";
                Log.e(TAG, String.valueOf(R.string.error_message));
                break;
        }
        return sortBy;
    }

    /**
     * Checks if there is internet permission.
     * @param context
     * @return if internet permission exists.
     */
    public static boolean checkInternetPermission(Context context){
        PackageManager pm = context.getPackageManager();
        int hasPermission = pm.checkPermission(Manifest.permission.INTERNET,context.getPackageName());
        return hasPermission == PackageManager.PERMISSION_GRANTED;
    }


}
