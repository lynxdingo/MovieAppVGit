package com.example.android.movieappv4.utilities;

import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.example.android.movieappv4.MovieSimple;
import com.example.android.movieappv4.database.DatabaseHandler;

import java.util.Arrays;
import java.util.List;

/**
 * Deals with Logs.
 */

public class LogUtils {

    public static void arrayLog(String name, String[] value, String TAG){
        String tempStr = Arrays.toString(value);
        Log.i(TAG,name+" = "+tempStr);
    }

    public static void stringLog(String name, String value, String TAG){
        Log.i(TAG,name+" = "+value);
    }

    public static void stringLog(String name, int value, String TAG){
        Log.i(TAG,name+" = "+String.valueOf(value));
    }

    public static void completedLog(String name, String TAG){
        Log.i(TAG,"Completed "+name+".");
    }

    public static void printDatabase(DatabaseHandler dbHelper){

        List<MovieSimple> movieSimple = dbHelper.getAllMovieSimples();

        try {
            for (MovieSimple ms : movieSimple) {
                String log = "Id: "+ms.getId()+" ,Title: " + ms.getTitle() + " ,Favorite: " + ms.getFavorite();
                // Writing Contacts to log
                Log.d("Name: ", log);
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            Log.e("DATABASE","SQL error",e);
        }

        if (movieSimple.size() == 0) Log.i("DATABASE","Database is empty.");
        else LogUtils.stringLog("db_size",movieSimple.size(),"DATABASE");
    }

}
