package com.example.android.movieappv4;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.movieappv4.database.DatabaseHandler;
import com.example.android.movieappv4.utilities.JsonUtils;
import com.example.android.movieappv4.utilities.LogUtils;
import com.example.android.movieappv4.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChildActivity extends AppCompatActivity {
    private final static String TAG = ChildActivity.class.getSimpleName();
    private MovieSimple movieSimples;
    private DatabaseHandler mDb;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_release_date)
    TextView tvReleaseDate;
    @BindView(R.id.tv_vote_average)
    TextView tvVoteAverage;
    @BindView(R.id.tv_plot_synopsis)
    TextView tvPlotSynopsis;
    @BindView(R.id.iv_movie_poster)
    ImageView ivMoviePoster;
    @BindView(R.id.bt_favorite)
    Button btFavorite;
    @BindView(R.id.bt_trailer)
    Button btTrailer;
    @BindView(R.id.lv_reviews)
    ListView lvReviews;

    @Override
    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        MovieReview[] movieReviews = null;
        String[] reviewsArray;


        movieSimples = getIntent().getParcelableExtra(Intent.EXTRA_TEXT);
        LogUtils.stringLog("movie_id",movieSimples.getId(),TAG);

        this.mDb = new DatabaseHandler(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);
        ButterKnife.bind(this);

        tvTitle.setText(movieSimples.getTitle());
        tvReleaseDate.setText(movieSimples.getRelease_date());
        tvVoteAverage.setText(movieSimples.getVote_average());
        tvPlotSynopsis.setText(movieSimples.getPlot_synopsis());

        isInFavorites();
        btTrailer.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        Uri movieImageUri = NetworkUtils.buildImageUri(movieSimples.getMovie_poster());
        Picasso.with(ChildActivity.this).load(movieImageUri)
                .placeholder(android.R.drawable.stat_notify_error)
                .error(R.drawable.broken_image)
                .into(ivMoviePoster);

        try{
            //reviews
            String reviewsStr = new MovieQueryTask()
                    .execute(movieSimples.getId(), NetworkUtils.BUILD_REVIEWS_URL)
                    .get(300, TimeUnit.SECONDS);
            reviewsArray = JsonUtils.getStringsFromJson(reviewsStr);

            if (reviewsArray.length>0){
                movieReviews = new MovieReview[reviewsArray.length];
                for (int i = 0; i < movieReviews.length;i++){
                    movieReviews[i] = new MovieReview(reviewsArray[i]);
                }
            }
        }catch(JSONException e){
            Log.e(TAG,"JSON error",e);
        }catch(Exception e){
            Log.e(TAG,"Event timed out",e);
        }


        lvReviews.setAdapter(new ItemListAdapter(this,movieReviews,0));
    }


    public void onClickOpenTrailerButton(View view) {
        String[] videosArray;

        try {
            //trailer
            String videosStr = new MovieQueryTask()
                    .execute(movieSimples.id, NetworkUtils.BUILD_VIDEOS_URL)
                    .get(300, TimeUnit.SECONDS);
            videosArray = JsonUtils.getStringsFromJson(videosStr);
            videosArray = JsonUtils.getTrailersFromVideos(videosArray);

            if (videosArray != null){
                JSONObject jsonObject = new JSONObject(videosArray[0]);
                String trailer = new MovieQueryTask()
                        .execute(jsonObject.getString(JsonUtils.TRAILER_KEY),NetworkUtils.BUILD_TRAILER_URL)
                        .get(300,TimeUnit.SECONDS);
                playMedia(trailer);
            }

        }catch(JSONException e){
            Log.e(TAG,"JSON error",e);
        }catch(Exception e){
            Log.e(TAG,"Event timed out",e);
        }

    }

    private void playMedia(String url) {
        Uri webPage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webPage);

        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }
    }


    /**
     * Changes button color when clicked on and add movie to favorites
     * @param view view
     */
    public void onClickAddFavoriteButton(View view){
        if (!mDb.inFavorite(movieSimples.getTitle())){//if not in favorites
            movieSimples.setFavorite("true");
            mDb.updateMovieSimple(movieSimples);
            Toast.makeText(this, "Adding \""+movieSimples.title+"\" to favorites...", Toast.LENGTH_SHORT).show();
        }else{// if in favorites
            movieSimples.setFavorite("false");
            mDb.updateMovieSimple(movieSimples);
            Toast.makeText(this, "Removing \""+movieSimples.title+"\" from favorites...", Toast.LENGTH_SHORT).show();
        }
        isInFavorites();

    }

    @SuppressWarnings("deprecation")
    private void isInFavorites(){
        //as long as movieSimple is not in database, add movie to database
        if (movieSimples.getFavorite()==null && !mDb.inFavorite(movieSimples.getTitle())){
            movieSimples.setFavorite("false");
            mDb.addMovieSimple(movieSimples);
            LogUtils.printDatabase(mDb);
        }

        //if inFavorite
        if (mDb.inFavorite(movieSimples.getTitle())){
            btFavorite.setBackgroundColor(getResources().getColor(R.color.colorAddedFavorite));
        }else{
            btFavorite.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }
}
