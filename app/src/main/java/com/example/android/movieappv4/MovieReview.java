package com.example.android.movieappv4;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Deals with Reviews.
 */

public class MovieReview {

    //Reviews
    private final static String REVIEW_AUTHOR = "author";
    private final static String REVIEW_CONTENT = "content";

    String mJson;
    String mAuthor;
    String mContent;

    public MovieReview(String json) throws JSONException{
        this.mJson = json;
        JSONObject jsonObject = new JSONObject(json);

        this.mAuthor = jsonObject.getString(REVIEW_AUTHOR);
        this.mContent = jsonObject.getString(REVIEW_CONTENT);
    }

    public String getReviewAuthor(){return mAuthor;}
    public String getReviewContent(){return mContent;}
}
