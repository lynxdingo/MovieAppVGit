package com.example.android.movieappv4;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A List for the review section in the ChildActivity.
 */
@SuppressWarnings("all")
public class ItemListAdapter extends BaseAdapter {
    private final static String TAG = ItemListAdapter.class.getSimpleName();
    Context mContext;
    int mNumberItems;
    MovieReview[] mMovieReviews;

    @Nullable @BindView(R.id.tv_review_author)TextView tvReviewAuthor;
    @Nullable @BindView(R.id.tv_review_content)TextView tvReviewContent;


    public ItemListAdapter(Activity a, MovieReview[] movieReviews, int numberOfItems){
        this.mContext = a;
        this.mMovieReviews = movieReviews;

        if (numberOfItems == 0){
            this.mNumberItems = getCount();
        }else{
            this.mNumberItems = numberOfItems;
        }
    }

    @Override
    public int getCount(){
        if (mMovieReviews != null){
        return mMovieReviews.length;
        }
        return -1;
    }

    @Override
    public Object getItem(int i) {
        if (i > getCount()-1 || getCount()==0){
            return null;
        }
        return mMovieReviews[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;

        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.item_review,null);
        }
        ButterKnife.bind(this,view);

        if (position < getCount()){
            tvReviewAuthor.setText(mMovieReviews[position].getReviewAuthor());
            tvReviewContent.setText(mMovieReviews[position].getReviewContent());
        }else if(getCount() == 0){
            Toast.makeText(mContext,"Internet Error!",Toast.LENGTH_LONG).show();
        }

        return view;
    }
}
