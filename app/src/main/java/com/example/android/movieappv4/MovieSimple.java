package com.example.android.movieappv4;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Holds all important information of a specific movie.
 */
@SuppressWarnings("all")
public class MovieSimple implements Parcelable{
    private final static String TAG = MovieSimple.class.getSimpleName();

    private final static String TK_POSTER_PATH = "poster_path";

    private final static String TK_OVERVIEW = "overview";
    private final static String TK_DATE = "release_date";
    private final static String TK_ID = "id";
    private final static String TK_TITLE = "title";
    private final static String TK_AVERAGE = "vote_average";

    String id;
    String title;
    String release_date;
    String movie_poster;
    String vote_average;
    String plot_synopsis;
    String reviews;
    String favorite;//in favorite = true, not in favorite = false

    JSONObject jsonObject;

    public MovieSimple(){}//empty constructor

    @Override
    public void writeToParcel(Parcel out, int flag){
        out.writeString(id);
        out.writeString(title);
        out.writeString(release_date);
        out.writeString(movie_poster);
        out.writeString(vote_average);
        out.writeString(plot_synopsis);
        out.writeString(reviews);
        out.writeString(favorite);
    }

    protected MovieSimple(Parcel in){
        id = in.readString();
        title = in.readString();
        release_date = in.readString();
        movie_poster = in.readString();
        vote_average = in.readString();
        plot_synopsis = in.readString();
        reviews = in.readString();
        favorite = in.readString();
    }


    public MovieSimple(String movieJson) throws JSONException{
        if (movieJson != null) {
            this.jsonObject = new JSONObject(movieJson);

            this.id = getTokenResult(TK_ID);
            this.title = getTokenResult(TK_TITLE);
            this.release_date = getTokenResult(TK_DATE);
            this.movie_poster = getTokenResult(TK_POSTER_PATH);
            this.vote_average = getTokenResult(TK_AVERAGE);
            this.plot_synopsis = getTokenResult(TK_OVERVIEW);
        }else{
            this.id = "-1";//an error has occurred
            Log.e(TAG,"MovieSimpleJson movie_id = "+id);
        }
    }

    @Override
    public int describeContents(){
        return 0;
    }

    public static final Parcelable.Creator<MovieSimple> CREATOR = new Parcelable.Creator<MovieSimple>(){

        @Override
        public MovieSimple createFromParcel(Parcel in){
            return new MovieSimple(in);
        }

        @Override
        public MovieSimple[] newArray(int size){
            return new MovieSimple[size];
        }
    };


    private String getTokenResult(String token) throws JSONException{
        return jsonObject.getString(token);
    }

    /**
     * Returns a String[] with all the data needed.
     * @return {id,title,release_date,movie_poster,vote_average,plot_synopsis}
     */
    public String[] getMovieSimpleInfo(){
        String[] infoStr = {id,title,release_date,movie_poster,vote_average,plot_synopsis};
        return infoStr;
    }

    /**
     * Returns the ID of the movie.
     * @return id in int
     */
    public int getMovieSimpleId(){

        if(id != null){
            return Integer.valueOf(id);
        }
        return -1;//id is null
    }

    //GETTER and SETTER
    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovie_poster() {
        return movie_poster;
    }

    public void setMovie_poster(String movie_poster) {
        this.movie_poster = movie_poster;
    }

    public String getPlot_synopsis() {
        return plot_synopsis;
    }

    public void setPlot_synopsis(String plot_synopsis) {
        this.plot_synopsis = plot_synopsis;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVote_average() {
        return vote_average;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }
}
