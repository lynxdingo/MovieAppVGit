package com.example.android.movieappv4;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;

import com.example.android.movieappv4.database.DatabaseHandler;
import com.example.android.movieappv4.utilities.JsonUtils;
import com.example.android.movieappv4.utilities.NetworkUtils;

import java.net.URL;

/**
 * An AsyncTask
 */
@SuppressWarnings("all")
public class MovieQueryTask extends AsyncTask<String, Void, String>  {
    private final String TAG = MovieQueryTask.class.getSimpleName();
    private String tempStr;//a temporal String for Log.INFO
    private Context mContext;
    private GridView mGridView;

    public MovieQueryTask(){}

    public MovieQueryTask(Activity a, GridView v){
        this.mContext = a;
        this.mGridView = v;
    }

    @Override
    protected String doInBackground(String... params) {
        String data;
        String connectedUrl = null;
        int option;

        //If there is no movie page, there is nothing to output.
        switch (params.length){
            case 1:
                data = params[0];
                option = 0;
                break;
            case 2:
                data = params[0];
                //if item chosen is favorite
                if (Integer.valueOf(data) == R.id.action_favorite){
                    return "favorite_database";
                }else{
                    option = Integer.valueOf(params[1]);
                }
                break;
            default:
                return null;
        }

        URL builtUrl = NetworkUtils.buildGeneralUrl(option,data);
        //for building a trailer a different method is needed
        if (option == Integer.valueOf(NetworkUtils.BUILD_TRAILER_URL)){
            connectedUrl = builtUrl.toString();
        }else{
            connectedUrl = respondedFromHttp(builtUrl);
        }
        return connectedUrl;
    }


    @Override
    protected void onPostExecute(String s) {
        try{
          if(mGridView!=null){
              if (s.equals("favorite_database")){
                  mGridView.setAdapter(new ImageAdapter(mContext, new DatabaseHandler(mContext)));
              }else{
                mGridView.setAdapter(new ImageAdapter(mContext, JsonUtils.getStringsFromJson(s)));
                }
              }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private String respondedFromHttp(URL builtUrl){
        String connectedUrl = null;
        try {
            connectedUrl = NetworkUtils.getResponseFromHttpUrl(builtUrl);
        }catch (Exception e){
            e.printStackTrace();
        }
        return connectedUrl;
    }
}
