Movie App
=========
Description:
------------
    
A movie app that presents the user in a grid layout the most popular movies 
at the moment. With the option to sort by top rate and my favorites. Beyond 
that when the user clicks on a movie poster, they will be presented with more 
information about the selected movie. They also have the option to add the 
selected movie as one of their favorites, which will then be presented when
choosing "favorite" under the sorting options.
   
How to implement:
-----------------
Add API_KEY in the **gradle.properties** like in the following:
    
```  
API_KEY=""
```

in the quotation marks one places one's API_KEY.

To run the app:
---------------

The .apk file can be found in the **APK** folder.
